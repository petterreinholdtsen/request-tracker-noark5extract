Create depot extraction from a request tracker instance using REST API
======================================================================

In Norway, public offices are required to store their information in a
well functioning local archive and send it to the national or a
regional archive when it is no longer used.  The required methods and
formats are regulated by law.  The specification documenting the
format is named `the Noark 5 standard`_.

This package provide a way to extract information from a running
`Request Tracker`_ instance in accordance with these regulations.

The RT data model is mapped to Noark 5 like this:

* Tickets become files (mappe)

* Attachments become basis entries (basisregistrering)

* Internal RT JSON metadata for Attachment become document description
  (dokumentbeskrivelse) attached to the basis entry

* Internal RT JSON metadata for Tickets become entries (registrering)
  attached to the ticket file.

Communication with the Request Tracker REST API is done using `the
python-rt module`_ available from github, a copy of the library is
included in this repository as lib/rt.py to avoid external
dependencies.

The latest version of this system is available from `the project pages on codeberg`.

.. _the Noark 5 standard: https://www.arkivverket.no/forvaltning-og-utvikling/noark-standarden/noark-5/noark5-standarden
.. _Request Tracker: https://bestpractical.com/request-tracker/
.. _the python-rt module: https://github.com/CZ-NIC/python-rt/
.. _the project pages on gitlab: https://codeberg.org/noark/request-tracker-noark5extract
