all:

# XML schemas from
# https://www.arkivverket.no/forvaltning-og-utvikling/noark-standarden/noark-5/xml-skjemaer-for-deponering-og-avlevering-fra-noark-5

metadatakatalog-5v4.xsd:
	wget -O $@ http://schema.arkivverket.no/N5/v4.0/metadatakatalog.xsd
arkivstruktur-5v4.xsd:
	wget -O $@ http://schema.arkivverket.no/N5/v4.0/arkivstruktur.xsd
	sed -i s/metadatakatalog.xsd/metadatakatalog-5v4.xsd/ $@

check: arkivstruktur-5v4.xsd metadatakatalog-5v4.xsd
	xmllint --valid --noout --schema arkivstruktur-5v4.xsd arkivstruktur/arkivstruktur.xml
