# Noark 5.5 alle krav med gruppering

## Overordnet datamodell

### Krav nr. 2.1.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For at en løsning skal kunne godkjennes etter Noark 5 må den konseptuelle modellen av arkivstrukturen og de funksjonelle muligheter den gir, kunne implementeres i det aktuelle systemets (fysiske) datastrukturer.

#### Merknad
Innebærer at det må implementeres slik at data skal kunne presenteres og hentes ut på den måten.

### Krav nr. 2.1.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Arkivdokumenter skal inngå i en arkivstruktur som minst inneholder følgende arkivenheter: 
arkiv, arkivdel, registrering, dokumentbeskrivelse og dokumentobjekt.


### Krav nr. 2.1.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Journalføringspliktige saksdokumenter skal inngå i et sakarkiv, med en arkivstruktur som minst skal inneholde følgende arkivenheter: 
arkiv, arkivdel, klassifikasjonssystem, klasse, mappe, registrering, dokumentbeskrivelse og dokumentobjekt.

#### Merknad
Obligatorisk for sakarkiver.

### Krav nr. 2.1.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For fysiske arkiver kan dokumentobjekt utgå.



## Metadata

### Krav nr. 2.2.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En Noark 5-løsning skal ha tjenester/funksjoner for å lagre, gjenfinne, endre og slette data og utvalg av data i henhold til metadatabeskrivelsene i alle arkivenheter og tilhørende klasser som er dokumentert i de konseptuelle modellene og metadatatabellene i Noark 5.

#### Merknad
Funksjonelle enkeltkrav i de forskjellige kapitlene kan overstyre dette kravet.

### Krav nr. 2.2.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivenhet skal kunne identifiseres entydig innenfor det arkivskapende organet. I et arkivuttrekk skal denne identifikasjonen hete systemID, og være entydig på tvers av alle uttrekk som organet produserer, dermed også på tvers av alle systemer organet benytter. Også arkivenheter som dupliseres i et arkivuttrekk, skal identifiseres entydig, slik at identiske arkivenheter har ulik systemID.



## Funksjonelle krav til arkiv

### Krav nr. 2.3.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom arkiver registrert som ”Avsluttet”, skal det ikke være mulig å legge til flere underliggende arkivdeler.

#### Merknad
Obligatorisk dersom arkivstatus brukes.

### Krav nr. 2.3.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når en tjeneste/funksjon sletter et helt Arkiv med alle underliggende nivå, skal dette logges.



## Funksjonelle krav til Underarkiv

### Krav nr. 2.3.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Systemet bør ha en tjeneste/funksjon for å angi et arkiv som Underarkiv til et arkiv.


### Krav nr. 2.3.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et Underarkiv skal kun opprettes og endres gjennom Administrasjonssystemet for Noark 5.

#### Merknad
Obligatorisk dersom underarkiv brukes.


## Funksjonelle krav til arkivdel

### Krav nr. 2.3.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når en tjeneste/funksjon sletter en arkivdel, skal dette logges.


### Krav nr. 2.3.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom arkivdel er registrert som avsluttet (avsluttetDato er satt) skal det ikke være mulig å legge til flere tilhørende mapper eller registreringer

#### Merknad
Obligatorisk for sakarkiv.


## Funksjonelle krav til klassifikasjonssystem

### Krav nr. 2.4.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å etablere hierarkiske klassifikasjonssystem.  B Obligatorisk for sakarkiv

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.4.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å etablere fasetterte, hierarkiske klassifikasjonssystem. Følgende er standard:

* K-kodenøkkelen

#### Merknad
Obligatorisk for sakarkiver i kommunesektoren.

### Krav nr. 2.4.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å etablere endimensjonale klassifikasjonssystem. Følgende er standard:

* Juridisk person (privatperson eller næring)

* Gårds- og bruksnummer

#### Merknad
Obligatorisk for sakarkiv


## Funksjonelle krav til klasse

### Krav nr. 2.4.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For at en klasse skal kunne tilordnes en mappe, må den ligge på nederste nivå i klassehierarkiet.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.4.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom verdien i klasse er registrert som avsluttet (avsluttetDato), skal det ikke være mulig å tilordne nye mapper til klassen.

#### Merknad
Obligatorisk dersom det er mulig å avslutte klasser.

### Krav nr. 2.4.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Bare autorisert personale kan opprette klasser. Andre brukere kan gis tillatelse til å opprette klasser.

#### Merknad
Obligatorisk for sakarkiv


## Strukturelle krav til mappe

### Krav nr. 2.5.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En mappe skal kunne være av forskjellig type. Dette er i den konseptuelle modellen løst gjennom spesialisering.


### Krav nr. 2.5.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En mappe som inneholder journalposter skal være en saksmappe.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.5.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En mappe som inneholder møteregistreringer bør være en møtemappe

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.5.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å definere relevante tilleggsmetadata for møtemappe i tillegg til de metadataene som er definert i standarden.


### Krav nr. 2.5.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en mappe er registrert som avsluttet (avsluttetDato) skal det ikke være mulig å legge flere registreringer til mappen.



## Funksjonelle krav til mappe

### Krav nr. 2.5.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom det er angitt et primært klassifikasjonssystem for arkivdel, skal alle mapper i arkivdelen ha verdier fra dette klassifikasjonssystemet som primær klasse.

#### Merknad
Obligatorisk dersom primært klassifikasjonssystem er angitt for arkivedel.


## Strukturelle krav til registrering

### Krav nr. 2.6.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En registrering skal kunne være av forskjellig type.
Dette er i den konseptuelle modellen løst gjennom spesialisering.


### Krav nr. 2.6.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Registrering av journalføringspliktige dokumenter skal løses gjennom journalpost.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.6.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Registrering av typen journalpost skal ha korrespondansepart.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 2.6.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Arkivering av saksdokumenter som ikke skal journalføres skal løses gjennom registrering av typen arkivnotat.

#### Merknad
Obligatorisk for arkivering uten journalføring i sakarkiver.

### Krav nr. 2.6.5 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Registrering av møtedokumenter bør løses gjennom møteregistrering.


### Krav nr. 2.6.6 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å definere relevante tilleggsmetadata for møteregistrering i tillegg til de metadataene som er definert i standarden.


### Krav nr. 2.6.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en registrering er registrert som arkivert (avsluttetDato er satt) skal det ikke være mulig å legge flere dokumentbeskrivelser til registreringen.



## Strukturelle krav til dokumentbeskrivelse og dokumentobjekt

### Krav nr. 2.7.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et dokumentobjekt som er tilknyttet samme dokumentbeskrivelse skal kunne referere til forskjellige versjoner av dokumentet


### Krav nr. 2.7.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et dokumentobjekt som er tilknyttet samme dokumentbeskrivelse skal kunne referere til forskjellige varianter av et dokument.


### Krav nr. 2.7.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et dokumentobjekt som er tilknyttet samme dokumentbeskrivelse skal kunne referere til samme dokument lagret i forskjellig format.



## Funksjonelle krav til dokumentbeskrivelse og dokumentobjekt

### Krav nr. 2.7.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjoner som ved opprettelse av nytt dokument skal knytte dette til en dokumentbeskrivelse.


### Krav nr. 2.7.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å opprette en dokumentbeskrivelse uten elektronisk dokument.


### Krav nr. 2.7.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en funksjon/tjeneste for å arkivere en eller flere versjoner/varianter/formater av et dokument.


### Krav nr. 2.7.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette et arkivert dokument. Eldre versjoner av dokumentet skal likevel kunne slettes.


### Krav nr. 2.7.8 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved tilknytning av et dokument til en registrering, skal det kunne angis om det er et hoveddokument eller et vedlegg (tilknyttetRegistreringSom).



## Overordnede krav til arkivuttrekk

### Krav nr. 2.7.9 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon som gjør det mulig for arkivadministrator å angi hvilke dokumentformater som er definert som arkivformater.


### Krav nr. 2.7.10 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon som gjør at arkivadministrator kan sette opp regler for når (hvilke statuser) arkivdokumenter skal konverteres til arkivformat.


### Krav nr. 2.7.11 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være konfigurerbart om dokumenter skal konverteres til arkivformat når status på dokumentbeskrivelse settes til ”Dokumentet er ferdigstilt”.


### Krav nr. 2.7.12 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være konfigurerbart om alle eller spesielt merkede versjoner skal konverteres til arkivformat.


### Krav nr. 2.7.13 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon og rapportering for filformattesting av dokumentene som er lagret i kjernen. Rapporten skal gi oversikt over hvilke mapper, registreringer og/eller dokumentbeskrivelser som ikke inneholder dokumenter lagret i godkjent arkivformat.



## Krav til sletting av dokumnetversjoner

### Krav nr. 2.7.14 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Autoriserte brukere skal kunne slette en arkivert inaktiv dokumentversjon. Den siste, endelige versjonen skal ikke kunne slettes.


### Krav nr. 2.7.15 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å søke fram dokumenter som er arkivert i flere versjoner.


### Krav nr. 2.7.16 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å utføre sletting av mange inaktive dokumentversjoner samtidig, f.eks. alle inaktive dokumentversjoner som funnet etter et søk.


### Krav nr. 2.7.17 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Sletting av arkiverte inaktive dokumentversjoner skal logges.



## Krav til sletting av dokumnetvarianter

### Krav nr. 2.7.18 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Autoriserte brukere skal kunne slette en arkivert dokumentvariant. Det siste endelige dokumentet i arkivformat skal ikke kunne slettes.


### Krav nr. 2.7.19 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å søke fram arkiverte dokumentvarianter.


### Krav nr. 2.7.20 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å slette mange dokumentvarianter samtidig, f.eks. alle dokumentvarianter som er funnet etter et søk.


### Krav nr. 2.7.21 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Sletting av arkiverte dokumentvarianter skal logges.



## Krav til sletting av dokumnetformater

### Krav nr. 2.7.22 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Autoriserte brukere skal kunne slette et arkivert dokument i produksjonsformat dersom dokumentet er blitt konvertert til arkivformat. Dokumentet i arkivformat skal ikke kunne slettes.


### Krav nr. 2.7.23 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å søke fram dokumenter arkivert i produksjonsformat.


### Krav nr. 2.7.24 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å slette mange produksjonsformater samtidig, f.eks. alle produksjonsformater som er funnet etter et søk.


### Krav nr. 2.7.25 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Sletting av arkiverte produksjonsformater skal logges.



## Funksjonelle krav til Skjerming

### Krav nr. 2.8.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Skjerming bør kunne arves fra overordnet nivå til ett eller flere underliggende nivå i arkivstrukturen.


### Krav nr. 2.8.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Arvede verdier skal kunne overstyres.


### Krav nr. 2.8.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å skjerme tittel i mappe helt eller delvis.



## Funksjonelle krav til Nøkkelord

### Krav nr. 2.8.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes en tjeneste/funksjon for å knytte ett eller flere nøkkelord til klasser, mapper og registreringer (unntatt registrering).



## Funksjonelle krav til Kryssreferanse

### Krav nr. 2.8.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon som kan lagre, gjenfinne, endre og slette en Kryssreferanse mellom:

* Mapper

* Registreringer
eller til referanser mellom disse.

#### Merknad
Obligatorisk for sakarkiv, aktuelt for mange fagsystemer.

### Krav nr. 2.8.5 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes en tjeneste/funksjon som kan lagre, gjenfinne, endre og slette en Kryssreferanse mellom:

* Klasser



## Funksjonelle krav til Merknad

### Krav nr. 2.8.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon som kan registrere en Merknad til mappe eller registrering.

#### Merknad
Obligatorisk for sakarkiv, aktuelt for mange fagsystemer.

### Krav nr. 2.8.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom mer enn én merknad er knyttet til en mappe eller en registrering, må metadataene grupperes sammen ved eksport og utveksling.

#### Merknad
Obligatorisk for sakarkiv, aktuelt for mange fagsystemer.

### Krav nr. 2.8.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig fritt å definere typer merknader.



## Funksjonelle krav til Part

### Krav nr. 2.8.9 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å tilegne mappe, registrering  eller dokumentbeskrivelse et fritt antall part

#### Merknad
Obligatorisk for løsninger hvor det inngår parter

### Krav nr. 2.8.10 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å ajourholde part for mappe, registrering og dokumentbeskrivelse

#### Merknad
Obligatorisk for løsninger hvor det inngår parter

### Krav nr. 2.8.11 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Part skal kunne skjermes helt eller delvis

#### Merknad
Obligatorisk for løsninger hvor det inngår parter


## Funksjonelle krav til Presedens

### Krav nr. 2.8.13 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å opprette en presedens knyttet til en sak eller en journalpost


### Krav nr. 2.8.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å opprette et register over hvilke verdier man skal kunne velge presedensHjemmel fra.


### Krav nr. 2.8.15 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å registrere tidligere presedenser, dvs. avgjørelser som ble tatt før man tok i bruk IKT-baserte løsninger for journalføring og arkivering.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.16 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å identifisere den eller de journalpostene i en saksmappe som inneholder presedensavgjørelsen.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.17 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Registrering, endring og tilgang til presedenser skal styres av tilgangsrettigheter.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.18 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Følgende statuser for Presedens er obligatoriske:

* ”Gjeldende”

* ”Foreldet”

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.19 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Foreldede presedenser skal ikke kunne slettes.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.20 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette en presedens selv om klassen som presedensen tilhører skal kasseres

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.21 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å etablere en samlet presedensoversikt i tilknytning til arkivstrukturen.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.22 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon som gir mulighet for å få en fullstendig oversikt over alle presedenser

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår

### Krav nr. 2.8.23 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Presedensvedtaket skal kunne presenteres i et offentlig dokument eller i en offentlig variant.

#### Merknad
Obligatorisk for løsninger hvor presedenser inngår


## Krav til administrasjon av kjernen

### Krav nr. 2.9.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å administrere kjernen


### Krav nr. 2.9.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det må kunne defineres minimum én bruker som er arkivadministrator, som kan logge seg eksplisitt på Noark 5 kjernen for å endre konfigurasjon og globale parametere


### Krav nr. 2.9.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for administrator for å opprette, redigere og slette arkivenheter (arkiv, arkivdel, klassifikasjonssystem, klasse, mappe, registrering, dokumentbeskrivelse og dokumentobjekt) og tilknyttede metadata som går utover de generelle begrensningene i kapittel 3.2. Slike registreringer skal logges.

#### Merknad
Obligatorisk dersom underarkiv brukes

### Krav nr. 2.9.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et arkiv og arkivets metadata skal kun opprettes gjennom Administratorfunksjonen for Noark 5 kjerne.


### Krav nr. 2.9.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et Underarkiv skal kun defineres og endres gjennom Administratorfunksjonen for Noark 5 kjerne.


### Krav nr. 2.9.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivdel og arkivdelens metadata skal kun opprettes og endres gjennom Administratorfunksjonen for Noark 5 kjerne.


### Krav nr. 2.9.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et klassifikasjonssystem og klassifikasjonssystemets metadata skal kun opprettes og endres gjennom Administratorfunksjonen for Noark 5 kjerne.


### Krav nr. 2.9.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å parameterstyre at status ”Dokumentet er ferdigstilt” skal settes automatisk på dokumentbeskrivelse ved andre statuser på mappe eller registrering


### Krav nr. 2.9.9 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kun autoriserte enheter, roller eller personer skal ha rett til å arkivere en ny versjon av et dokument på en registrering med status ekspedert, journalført eller avsluttet.


### Krav nr. 2.9.10 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kun autoriserte roller, enheter og personer skal kunne slette inaktive versjoner, varianter og formater av et dokument



## Overordnet krav til dokumnetfangst 

### Krav nr. 3.1.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjonalitet for fangst av elektroniske dokumenter uavhengig av filformat, metoder for teknisk koding, kilder eller andre tekniske egenskaper.


### Krav nr. 3.1.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal foreligge funksjonalitet som dokumenterer når en registrering er arkivert i eller innenfor Noark-systemet.


### Krav nr. 3.1.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dokumentfangsten skal skje på en slik måte at dokumentets innholds integritet blir opprettholdt. Løsningen må ha funksjonalitet som hindrer at noe eller noen kan endre innholdet i dokumentet ved fangst. Dette gjelder også metadata.


### Krav nr. 3.1.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dokumentfangsten bør skje på en slik måte at dokumentets utseende (visuelle integritet) blir opprettholdt


### Krav nr. 3.1.5 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes funksjonalitet for helautomatisk dokumentfangst


### Krav nr. 3.1.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved helautomatisk dokumentfangst skal det være mulig å knytte alle obligatoriske metadata til dokumentet.

#### Merknad
Obligatorisk ved helautomatisk dokumentfangst

### Krav nr. 3.1.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved helautomatisk dokumentfangst skal det være mulig å knytte dokumenter til et klassifikasjonssystem

#### Merknad
Obligatorisk ved helautomatisk dokumentfangst

### Krav nr. 3.1.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved helautomatisk dokumentfangst bør det være mulig å knytte dokumenter til relevante deler av arkivstrukturen


### Krav nr. 3.1.9 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være begrensninger i antall dokumenter som kan bli arkivert i løsningen.


### Krav nr. 3.1.10 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjoner for å sikre at alle komponenter i et sammensatt dokument fanges.


### Krav nr. 3.1.11 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjoner for å sikre at et sammensatt elektronisk dokument håndteres som en enhet, hvor relasjonen mellom komponentene og dokumentets indre struktur opprettholdes.

#### Merknad
Obligatorisk hvis løsningen håndterer sammensatte dokumenter


## Krav til metadata for dokumenter mottatt eller sendt med elektronisk signatur

### Krav nr. 3.1.12 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Elektronisk dokument som mottas i kryptert form, skal dekrypteres ved mottak. Metadata om sikkerhetsnivå og verifikasjon av uavviselighet/ikke-benektbarhet skal lagres med registrering eller dokumentbeskrivelse.

#### Merknad
Obligatorisk for arkiver som mottar krypterte dokumenter

### Krav nr. 3.1.13 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når et elektronisk dokument sendes ut fra organet i kryptert form, skal metadata om sikkerhetsnivå og verifikasjon av uavviselighet/ikke-benektbarhet lagres med registreringen.

#### Merknad
Obligatorisk for arkiv som sender krypterte dokumenter

### Krav nr. 3.1.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
På følgende nivåer i arkivstrukturen bør arkivadministrator kunne angi hvilket sikkerhetsnivå som skal kreves, og hvorvidt elektronisk signatur skal kreves, for inngående dokumenter:

* Arkiv

* Arkivdel

* Klassifikasjonssystem

* Mappe


### Krav nr. 3.1.15 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
På følgende nivåer i arkivstrukturen bør arkivadministrator kunne angi hvilket sikkerhetsnivå som skal brukes, og om elektronisk signatur skal brukes, ved elektronisk utsending av dokumenter:

* Arkiv

* Arkivdel

* Klassifikasjonssystem

* Mappe


### Krav nr. 3.1.16 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-løsningen skal kunne konfigureres slik at alle dokumenter som sendes eller mottas kryptert blir lagret i ikke‑kryptert form i arkivet.

#### Merknad
Obligatorisk for arkiver som mottar eller sender krypterte dokumenter

### Krav nr. 3.1.17 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-løsningen bør kunne konfigureres slik at dokumenter som sendes eller mottas kryptert også blir lagret kryptert i arkivet


### Krav nr. 3.1.18 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom løsningen tillater at dokumenter lagres i kryptert form, må det lagres tilstrekkelige metadata til at en autorisert bruker kan dekryptere dokumentet ved behov

#### Merknad
Obligatorisk for løsninger som tillater lagring av krypterte dokumenter


## Krav til tjenestegrensesnitt

### Krav nr. 3.1.19 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For løsninger hvor Noark-kjernen skal integreres med fagsystem med forenklet sakarkiv funksjonalitet, kan man velge GeoIntegrasjonsstandarden som tjenestegrensesnitt.


### Krav nr. 3.1.20 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For løsninger hvor Noark-kjernen skal ha en fullstendig integrasjon med fagsystemet bør Noark 5 tjenestegrensenitt brukes.



## Krav til masseimport utløst fra Noark 5-kjerne 

### Krav nr. 3.1.21 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-løsningen bør inneholde masseimportfunksjonalitet som henter dokumenter fra en angitt plassering og knytte disse til klasser, mapper, registreringer eller dokumentbeskrivelser.


### Krav nr. 3.1.22 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved masseimport bør det være mulig å velge om alle importerte dokumenter skal knyttes til én og samme arkivenhet på samme nivå i arkivstrukturen eller om hvert enkelt dokument skal knyttes til forskjellige arkivenheter i arkivstrukturen.


### Krav nr. 3.1.23 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved masseimport bør det være mulig å knytte importerte dokumenter til en allerede eksisterende klasse, mappe, registrering eller dokumentbeskrivelse.


### Krav nr. 3.1.24 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved masseimport bør det være mulig å definere og utfylle metadatasettet for dokumentene som skal importeres, kun én gang.


### Krav nr. 3.1.25 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-kjernen bør ha automatikk for å fange dokumenter som er generert og overført fra andre system.


### Krav nr. 3.1.26 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-kjernen bør ha mulighet til å håndtere input kø ved masseimport.

Merknad: For håndtering av input køen kan det for eksempel være ønskelig å se køene, pause en eller flere køer, starte en eller alle køene på nytt, slette en kø.


### Krav nr. 3.1.27 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Noark 5-kjernen bør kunne fange metadata knyttet til alle dokumentene som overføres, automatisk. Det bør være mulig å overstyre dette ved manglede eller feil metadata.


### Krav nr. 3.1.28 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved automatisert masseimport, skal det være funksjonalitet for å validere metadata med tilhørende dokumenter automatisk, for å sikre opprettholdt dataintegritet.

#### Merknad
Obligatorisk for funksjon for automatisert masseimport

### Krav nr. 3.1.29 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved masseimport skal det være mulig å importere logginformasjon om de importerte dokumentene, og logginformasjonen skal inngå i importen som eget (egne) dokument.

#### Merknad
Obligatorisk for funksjon for automatisert masseimport


## Krav til frysing av metadata for mappe

### Krav nr. 3.2.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å avslutte en mappe (dvs. at avsluttetDato settes).


### Krav nr. 3.2.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en mappe som er avsluttet skal det ikke være mulig å endre følgende metadata:

* tittel

* dokumentmedium


### Krav nr. 3.2.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette en mappe som er avsluttet.


### Krav nr. 3.2.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å legge til flere registreringer i en mappe som er avsluttet



## Krav til frysing av metadata for mappe

### Krav nr. 3.2.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En Saksmappe avsluttes ved at saksstatus settes til «Avsluttet».

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å avslutte en Saksmappe uten at det er angitt en primær klassifikasjon (klasse).

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å avslutte en Saksmappe som inneholder Journalposter som ikke er arkivert (dvs. som har status «Arkivert»).

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å avslutte en Saksmappe uten at alle dokumenter på registreringene i mappen er lagret i godkjent arkivformat.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.9 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å avslutte en Saksmappe uten at alle restanser på Journalposter i mappen er avskrevet (ferdigbehandlet).

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.10 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når statusen til en Saksmappe settes til avsluttet, skal det på mappenivå ikke være mulig å endre metadataene:

* saksdato

* administrativEnhet

* saksansvarlig

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.11 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En avsluttet Saksmappe bør kunne åpnes igjen av autoriserte brukere. Åpning av mappe skal logges.


### Krav nr. 3.2.12 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette en Saksmappe som inneholder Journalposter med status som er ferdigstilt (dvs. Ekspedert, Journalført eller Arkivert).

#### Merknad
Obligatorisk for sakarkiv


## Krav til frysing av metadata for registrering

### Krav nr. 3.2.13 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å arkivere en registrering (dvs. at arkivertDato settes)


### Krav nr. 3.2.14 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en registrering som er arkivert skal det ikke være mulig å endre følgende metadata:

* tittel

* dokumentmedium

* referanseArkivdel


### Krav nr. 3.2.15 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når en registrering er arkivert bør det for autoriserte brukere fortsatt være mulig å endre de øvrige metadataene på registrering. Endringer skal logges.


### Krav nr. 3.2.16 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette en registrering som er arkivert.


### Krav nr. 3.2.17 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en registrering er arkivert, skal det ikke være mulig å legge til flere dokumentbeskrivelser.



## Krav til frysing av metadata for mappe

### Krav nr. 3.2.18 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når status på Journalpost settes til «Arkivert», skal arkivertDato settes automatisk.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.19 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å slette en Journalpost som har eller har hatt status «Ekspedert», «Journalført», «Arkivert» eller «Utgår».

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.20 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør ikke være mulig å slette en Journalpost med status ”Ferdigstilt fra saksbehandler” eller ”Godkjent av leder”.


### Krav nr. 3.2.21 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å slette en Journalpost med status «Reservert dokument».


### Krav nr. 3.2.22 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For Journalpost av typen «inngående dokument» med status «journalført» skal det ikke tillates å endre følgende metadata:

* løpenummer

* mottattdato

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.23 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For Journalpost av typen «inngående dokument» med status «arkivert» skal det på Journalpost ikke være mulig å endre følgende metadata:

* journalposttype

* journaldato

* dokumentetsDato

* korrespondansepart

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.24 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For Journalpost av typer egenproduserte dokumenter («utgående dokument», «organinternt dokument for oppfølging», «organinternt dokument uten oppfølging») med status ”Ekspedert”, ”Journalført” eller ”Arkivert”, skal det på Journalpost ikke være mulig å endre følgende metadata:

* løpenummer

* journalposttype

* dokumentetsDato

* sendtDato

* saksbehandler

* administrativEnhet

* tittel

* korrespondansepart

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.25 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For Journalpost av typen «inngående dokument» med status ”midlertidig registrert” eller ”registrert av saksbehandler” bør alle metadata kunne endres.


### Krav nr. 3.2.26 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For Journalpost av typer egenproduserte dokumenter (”utgående dokument”, ”Organinternt dokument for oppfølging”, ”Organinternt dokument uten oppfølging”) med status ”Registrert av saksbehandler” og ”Ferdigstilt fra saksbehandler” bør det for autorisert personale være mulig å endre alle metadata.


### Krav nr. 3.2.27 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å arkivere en ny variant av et dokument på en Journalpost med status ”Ekspedert”, ”Journalført” eller ”Arkivert”, uten å måtte reversere statusen. Denne varianten må ikke kunne forveksles med den ferdigstilte varianten som ble ekspedert



## Krav til frysing av dokument og metadata for dokumentbeskrivelse

### Krav nr. 3.2.28 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata for dokumentbeskrivelse for hoveddokument bør kunne fylles ut automatisk på basis av metadata fra registrering ved oppretting.


### Krav nr. 3.2.29 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å registrere at et dokument er i papirform og hvor det er lokalisert


### Krav nr. 3.2.30 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å sette journalstatus ”Ekspedert”, ”Journalført” eller ”Arkivert” dersom ikke dokumentstatus er satt til ”Dokumentet er ferdigstilt”.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.31 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å endre innholdet i et dokument når status på dokumentbeskrivelse er satt til ”Dokumentet er ferdigstilt”.


### Krav nr. 3.2.32 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør ikke være mulig å endre (reversere) status ”Dokumentet er ferdigstilt”.


### Krav nr. 3.2.33 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For dokumentbeskrivelse med status ”Dokumentet er ferdigstilt” skal det være tillatt å endre tittelen på hoveddokument og vedlegg.



## Krav til oppsplitting og sammenslåing av mapper, flytting av registreringer

### Krav nr. 3.2.35 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å flytte en registrering fra en mappe til en annen mappe.


### Krav nr. 3.2.36 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hvis registreringsID på registrering i et sakarkiv benytter det anbefalte formatet åå/nnnnnn-nnnn (dvs. kombinasjonen av saksnummer (mappeID) og dokumentnummer i saken), bør registreringsID endres automatisk. Registreringen bør automatisk tildeles første ledige dokumentnummer i mappen den flyttes til.


### Krav nr. 3.2.37 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Registreringer som ikke flyttes i mappe det flyttes registreringer fra, bør ikke få endret registreringsID.


### Krav nr. 3.2.38 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å flytte flere registreringer som er tilknyttet samme mappe i en samlet operasjon.


### Krav nr. 3.2.39 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å flytte en registrering hvis denne avskriver eller avskrives av andre registreringer som ikke flyttes. Hvis dette forsøkes skal brukeren få melding om hvilke koblinger som sperrer mot flytting

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.2.40 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Flytting av arkivert registrering skal være rollestyrt.


### Krav nr. 3.2.41 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å parameterstyre at alle brukere kan flytte registreringer de selv er saksbehandler for, hvis status er ”midlertidig registrert” eller ”registrert av saksbehandler”.


### Krav nr. 3.2.42 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved flytting og renummerering skal bruker få påminnelser om å endre nødvendige referanser på fysiske dokumenter i arkivet

#### Merknad
Obligatorisk for sakarkiv


## Krav til Dokumentflyt

### Krav nr. 3.3.1 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et dokument som er under produksjon, bør kunne sendes fram og tilbake i linjen det nødvendige antall ganger.


### Krav nr. 3.3.2 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Autoriserte roller og personer bør kunne se hvor dokumentet befinner seg til enhver tid.


### Krav nr. 3.3.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dokumentet bør bli sperret for endringer når det (videre)sendes, ev. det opprettes en ny versjon ved hver (videre)forsendelse.


### Krav nr. 3.3.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å registrere merknader til dokumentflyten.


### Krav nr. 3.3.5 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Mottaker av et dokument på flyt, bør bli varslet om at han/hun har mottatt et dokument.


### Krav nr. 3.3.6 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å gi en forpliktende «signatur» i alle ledd.


### Krav nr. 3.3.7 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å sende et dokument som er under produksjon, til trinnvis godkjenning (sekvensielt)


### Krav nr. 3.3.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å sende et dokument som er under produksjon, til høring til flere samtidig (parallelt)


### Krav nr. 3.3.9 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For dokument som er under produksjon, og som sendes på sekvensiell eller parallell dokumentflyt, bør det kunne parameterstyres om det automatisk skal opprettes nye versjoner for alle mottakere i flyten.


### Krav nr. 3.3.10 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne parameterstyres om versjonering skal forekomme bare for enkelte roller, enheter, grupper eller personer. Dette skal kunne gjøres fast eller på ad-hoc-basis.



## Krav til Avskrivning

### Krav nr. 3.4.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjoner for å få informasjon om restanser.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste/funksjon for å avskrive en registrering (Journalpost).

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å avskrive en inngående journalpost med èn eller flere utgående journalposter.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å la en utgående journalpost avskrive flere inngående journalposter.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når statusen til en mappe settes til avsluttet, skal alle uavskrevne Journalposter av typen ”inngående dokument” eller ”organinternt dokument for oppfølging” som er knyttet til mappen, avskrives med sak avsluttet

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes funksjonalitet for at avskriving av organinterne dokument som skal følges opp, skal kunne utføres for hver enkelt mottaker for seg. Dette innebærer at et mottatt, organinternt dokument kan være avskrevet for noen mottakere, men ikke for andre.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom et innkommet dokument avskrives av et utgående dokument, skal det være referanse mellom de to dokumentene.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom et notat avskrives av et annet notat, skal det være referanse mellom de to notatene.

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 3.4.9 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Avskrivning bør ikke registreres på kopimottakere.



## Krav nr. Krav til rapporten Restanseliste

### Krav nr. 3.4.10 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten bør kunne selekteres på følgende metadataelementer

* journaldato fra Journalpost (intervall bør kunne angis) og

* journalposttype fra Journalpost
 
* journalenhet

* administrativEnhet (Her bør det kunne angis om underliggende enheter skal inkluderes).

* avskrivingsmåte (Her bør det kunne velges mellom uavskrevne dokumenter og uavskrevne og foreløpig avskrevne dokumenter (verdi ***).

* kopimottaker. Det bør kunne angis om kopimottakere skal inkluderes eller ikke.


### Krav nr. 3.4.11 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold:
Følgende metadataelementer bør være med i rapporten, så fremt de finnes i løsningen:

Saksmappeinformasjon
Fra Saksmappe:
mappeID
tittel
administrativEnhet
saksansvarlig
journalenhet
Fra klasse
klasseID og tittel

Journalpostinformasjon
Fra Journalpost:
registreringsID
journaldato
dokumentetsDato (tekst ”Udatert” hvis dato mangler)
tittel
forfallsdato
korrespondanseparttype
korrespondansepartNavn
administrativEnhet
Saksbehandler



## Krav til rapporten Forfallsliste

### Krav nr. 3.4.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal kunne selekteres på følgende metadataelementer

* journaldato fra Journalpost (intervall skal kunne angis) og

* journalposttype fra Journalpost

* journalenhet

* administrativEnhet (Her skal det kunne angis om underliggende enheter skal inkluderes).

* kopimottaker: Det skal kunne angis om kopimottakere skal inkluderes eller ikke.

* forfallsdato i Journalpost (intervall skal kunne angis)


### Krav nr. 3.4.15 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold:
Rapporten skal inneholde følgende opplysninger, så fremt de finnes i løsningen:
Saksmappeinformasjon
Fra Saksmappe:
mappeID
tittel
administrativEnhet
saksansvarlig
journalenhet
Fra klasse
klasseID og tittel
Journalpostinformasjon
Fra Journalpost:
registreringsID
journaldato
dokumentetsDato (tekst ”Udatert” hvis dato mangler)
tittel
forfallsdato
korrespondanseparttype
korrespondansepartNavn
administrativEnhet
saksbehandler



## Krav til sikkerhet i kjernen

### Krav nr. 4.1.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle moduler eller systemer utenfor kjernen, som skal kommunisere med eller ha tilgang til objekter i Noark 5 kjerne, skal være identifisert og gjenkjennes av kjernen


### Krav nr. 4.1.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En ekstern modul som ikke lenger skal ha tilgang til tjenester skal fortsatt være identifisert i kjernen, men med en status som indikerer at den er ”passiv”


### Krav nr. 4.1.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en oversikt over hvilket eller hvilke tidsrom hver ekstern modul har vært aktiv


### Krav nr. 4.1.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det må kunne defineres minimum én bruker som er arkivadministrator, som kan logge seg eksplisitt på Noark 5 kjernen for å endre konfigurasjon og globale parametere


### Krav nr. 4.1.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Påloggingsidentifikator for en arkivadministrator som ikke lenger skal ha tilgang til kjernen skal kunne settes til status ”passiv”, som ikke gir muligheter for å logge på


### Krav nr. 4.1.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en oversikt over hvilket eller hvilke tidsrom påloggingsidentifikatoren har vært aktiv


### Krav nr. 4.1.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Minstekrav til autentiseringsstyrke for pålogging som arkivadministrator er passord, der det kan angis krav til passordets styrke (kompleksitet, lengde, varighet etc.)


### Krav nr. 4.1.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne brukes andre og sterkere autentiseringsmåter som alternativ til passord



## Krav til sikkerhetskonfigurasjon

### Krav nr. 4.1.9 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en arkivdel bør det kunne angis hvilken eller hvilke autentiseringsmåte(r) som kreves for de eksterne moduler som skal gis tilgang til å bruke tjenester i kjernen


### Krav nr. 4.1.10 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en arkivdel bør det kunne angis om bare den enkelte eksterne modul skal identifiseres, eller om det også kreves at hver enkelt personlige bruker identifiseres i kjernen


### Krav nr. 4.1.11 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en arkivdel bør det kunne angis om den modulen, eller alternativt den personlige brukeren, som er registrert som ansvarlig for en mappe eller en registrering skal ha lese- og redigeringstilgang til mappen eller registreringen automatisk, eller om det kreves eksplisitt rettighets­angivelse også for den som er mappe/registrerings­ansvarlig


### Krav nr. 4.1.12 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en arkivdel bør det kunne angis om tilgangsrettigheter arves nedover i hierarkiet som standard, eller om det må angis eksplisitte tilgangsrettigheter på hvert nivå


### Krav nr. 4.1.13 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en arkivdel bør det kunne angis om det skal tillates å angi at alle autentiserte eksterne moduler – både nåværende og fremtidige – har lese- eller redigerings-tilgang til et objekt.
(Dersom denne anbefalingen ikke implementeres, skal det forstås slik at det ikke tillates å angi at alle moduler har tilgang, men at bare konkret angitte moduler har tilgang til et objekt)



## Krav til rettighetsangivelser

### Krav nr. 4.1.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse skal det kunne registreres hvilke eksterne moduler som har lesetilgang


### Krav nr. 4.1.15 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse skal det kunne registreres hvilke eksterne moduler som har skrivetilgang

#### Merknad
Obligatorisk hvis krav 4.1.13 oppfylles

### Krav nr. 4.1.16 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse bør det være anledning til å angi lesetilgang for ”alle” eksterne moduler (både nåværende og fremtidige).


### Krav nr. 4.1.17 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse skal det være anledning til å angi oppdaterings­tilgang for ”alle” eksterne moduler (både nåværende og fremtidige).


### Krav nr. 4.1.18 () Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse bør det kunne registreres hvilke personlig identifiserte brukere som har lesetilgang


### Krav nr. 4.1.19 () Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver arkivdel, klasse, mappe, registrering og dokumentbeskrivelse bør det kunne registreres hvilke personlig identifiserte brukere som har oppdateringstilgang



## Kjernens krav til administrativ oppbygging

### Krav nr. 4.2.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle administrative enheter som skal ha tilgang til objekter i kjernen, skal være identifisert og gjenkjennes av kjernen.

#### Merknad
Obligatorisk for løsninger hvor administrative enheter skal ha tilgang til objekter i kjernen

### Krav nr. 4.2.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En administrativ enhet som ikke lenger skal ha tilgang til objekter i kjernen, skal fortsatt være identifisert i kjernen, men med en status som indikerer at den er ”passiv”.

#### Merknad
Obligatorisk for løsninger hvor administrative enheter skal ha tilgang til objekter i kjernen

### Krav nr. 4.2.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en oversikt over hvilket eller hvilke tidsrom hver administrative enhet har vært aktiv.

#### Merknad
Obligatorisk for løsninger hvor administrative enheter skal ha tilgang til objekter i kjernen


## Kjernens krav til Brukeradministrasjon

### Krav nr. 4.3.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle brukere som skal ha tilgang til enheter i kjernen, skal være identifisert og gjenkjennes av kjernen.

#### Merknad
Obligatorisk for løsninger hvor personlig identifiserte brukere skal være identifisert i kjernen

### Krav nr. 4.3.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kjernen skal kunne gjenkjenne i hvilken administrativ sammenheng brukeren virker til enhver tid.

#### Merknad
Obligatorisk for løsninger hvor personlig identifiserte brukere skal være identifisert i kjernen

### Krav nr. 4.3.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En bruker som ikke lenger skal ha tilgang til enheter i kjernen skal fortsatt være identifisert i kjernen, men med en status som indikerer at den er ”passiv”

#### Merknad
Obligatorisk for løsninger hvor personlig identifiserte brukere skal være identifisert i kjernen

### Krav nr. 4.3.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en oversikt over hvilket eller hvilke tidsrom hver bruker har vært aktiv.

#### Merknad
Obligatorisk for løsninger hvor personlig identifiserte brukere skal være identifisert i kjernen


## Krav til identifisering av brukere

### Krav nr. 4.4.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle brukere som skal ha tilgang til Noark 5-løsningen må være individuelt identifisert, og autentisert i tilstrekkelig grad


### Krav nr. 4.4.2 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ekstern katalog over identifiserte brukere kan brukes, i stedet for eksplisitt pålogging til Noark 5-løsningen


### Krav nr. 4.4.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Brukeren kan være pålogget en tilknyttet ekstern løsning, og la den eksterne løsningen ta hånd om hvilke rettigheter brukeren skal ha


### Krav nr. 4.4.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Brukeren kan være pålogget i løsningens driftsmiljø, og ha definert tilgangsrettigheter i en ressurskatalog. Noark 5- løsningen kan da brukes så langt de eksternt definerte tilgangsrettighetene rekker (”single sign-on”)



## Krav til autentiseringsstyrke

### Krav nr. 4.4.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Minstekravet til autentiseringsstyrke for pålogging som gir tilgang til Noark 5-løsningen er personlig passord for den individuelle bruker


### Krav nr. 4.4.6 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne angis krav til passordets styrke (kompleksitet, lengde, varighet/krav til hyppighet for passordskifte etc.)


### Krav nr. 4.4.7 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne brukes andre og sterkere autentiseringsmåter som alternativ til passord


### Krav nr. 4.4.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom løsningen gir mulighet for sterkere autentisering enn passord, må det også kunne stilles krav til en sterkere autentisering for at påloggingen skal aksepteres

#### Merknad
Obligatorisk hvis kravet over oppfylles


## Krav til håndtering av historiske brukeridentiteter

### Krav nr. 4.4.9 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En påloggingsidentifikator (”brukerident”) som ikke lenger skal ha tilgang til løsningen bør kunne settes til status ”passiv”, som ikke gir muligheter for å logge på


### Krav nr. 4.4.10 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en oversikt over hvilket eller hvilke tidsrom brukeridenten har vært aktiv

#### Merknad
Obligatorisk hvis kravet over oppfylles

### Krav nr. 4.4.11 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Brukerens ”fulle navn”, og eventuelle initialer som brukes til å identifisere brukeren som saksbehandler i dokumenter og skjermbilder, bør kunne endres for en gitt brukerident. Endring av navn og initialer for en brukerident er bare aktuelt dersom samme person skifter navn, og ikke for å tildele en tidligere brukt identifikator til en annen person. Gjenbruk av brukerID til andre brukere vanskeliggjør tolking av logg


### Krav nr. 4.4.12 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved en eventuell adgang til å endre ”fullt navn” og/eller initialer for en gitt påloggingsidentifikator, må alle navn og initialer kunne bevares i løsningen sammen med opplysninger om hvilket eller hvilke tidsrom de ulike navn eller initialer var i bruk

#### Merknad
Obligatorisk hvis kravet over oppfylles


## Krav til grunnprinsipp for autorisering

### Krav nr. 4.5.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
All redigerings- og skrivetilgang i Noark 5-løsningen skal være basert på et ”need to know” grunnprinsipp

#### Merknad
Obligatorisk der det gis slik tilgang fra ekstern modul

### Krav nr. 4.5.2 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et ”need to protect” grunnprinsipp kan velges for lesetilganger i en eller flere eksterne løsninger



## Krav til funksjonelle roller

### Krav nr. 4.5.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke kunne opprettes roller som opphever de generelle begrensninger som er definert i løsningen


### Krav nr. 4.5.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ulike kombinasjoner av funksjonelle krav som stilles til brukerens autorisasjon bør kunne settes sammen til forskjellige funksjonelle roller, som uttrykker typiske stillingskategorier eller oppgaveporteføljer i virksomheten


### Krav nr. 4.5.5 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver funksjonelle rolle bør det være mulig å definere et regelsett for prosessrelaterte rettigheter (jf. tabellen nedenfor)


### Krav nr. 4.5.6 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En bruker bør kunne ha flere ulike roller



## Krav til prosessrelaterte funksjonelle rettigheter og begrensninger

### Krav nr. 4.5.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rolleprofilens regelsett skal ikke kunne utvide de generelle funksjonelle rettighetene. Det er bare avgrensninger fra de tilgangsrettighetene en bruker ellers har, som skal kunne uttrykkes


### Krav nr. 4.5.8 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et regelsett bør kunne angi tillatte handlinger på bakgrunn av mappens status, registreringens status, dokumentbeskrivelsens status eller dokumentets status


### Krav nr. 4.5.9 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et regelsett bør kunne angi tillatte handlinger på bakgrunn av andre metadata som uttrykkes gjennom stringente, faste kodeverdier


### Krav nr. 4.5.10 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Regler i et regelsett bør kunne uttrykke et krav til oppgavedifferensiering (”separation of duties”), slik at det kan stilles krav til at flere enn én bruker godkjenner en bestemt handling


### Krav nr. 4.5.11 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En regel om oppgavedifferensiering kan stille betingelser om at en handling konfirmeres før den gjennomføres endelig. Det bør kunne stilles ulike typer krav til hvem som kan konfirmere handlingen, for eksempel en av følgende personer:

* Hvilken som helst annen autorisert bruker

* En bruker med en konkret angitt rolle (for eksempel ”leder” eller ”kontrollør”)

* Konkret angitt annen bruker, som er registrert som kontrasignerende på mappe- eller registreringsnivå


### Krav nr. 4.5.12 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Regler i et regelsett bør kunne uttrykke et krav til at partens samtykke innhentes og registreres for å tillate bestemte handlinger. Kravet er mest relevant for avgivelse av opplysninger til tredjepart, i tilfeller hvor adgangen til utlevering ellers ville ha vært begrenset av taushetsplikt


### Krav nr. 4.5.13 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et innhentet samtykke kan registreres konkret for den enkelte hendelsen, eller gis som ”stående samtykke” (vedvarende) for alle opplysninger i en sak


### Krav nr. 4.5.14 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom det er gitt et ”stående samtykke” skal det finnes funksjoner for å trekke samtykket tilbake igjen

#### Merknad
Obligatorisk hvis 4.5.13 oppfylles

### Krav nr. 4.5.15 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en part er autentisert som ekstern bruker med anledning til å registrere opplysninger i et fagsystem, bør det være mulig for vedkommende selv å registrere og trekke tilbake samtykke



## Krav til avgrensninger av autorisasjonenes nedslagsfelt, tilganger til data

### Krav nr. 4.5.16 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses innen angitt element i arkivstrukturen, ett av følgende:

* Hele Noark 5-løsningen

* Logisk arkiv

* Arkivdel

* Mappe

* Registrering


### Krav nr. 4.5.17 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses innen angitte organisatoriske grenser, en av følgende:

* Hele virksomheten

* Egen administrativ enhet uten underliggende enheter

* Egen administrativ enhet og underliggende enheter

* Navngitt annen administrativ enhet


### Krav nr. 4.5.18 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses til visse klassifiseringsverdier innen et klassifiseringssystem


### Krav nr. 4.5.19 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses til visse saksområder eller sakstyper, og/eller bare til saker produsert av et konkret angitt fagsystem


### Krav nr. 4.5.20 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses til særskilte egenskaper ved sakens parter. Slike begrensninger kan for eksempel gjelde:

* Partens geografiske tilhørighet (bosted, virksomhetsadresse etc.) etter postnummer, kommuner, fylker eller lignende

* Andre definerte partskategorier, som kan fremgå av eksterne parts- eller avsender/mottakerkataloger, for eksempel næringskategori, sivilstatus, alderstrinn, yrke osv.

* Konkret registrert tilordning av den enkelte part/klient mot en bestemt saksbehandler eller administrativ enhet


### Krav nr. 4.5.21 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangene for en bruker i en rolle bør kunne avgrenses til graderingskoder som er angitt på sak, journalpost eller dokument, slik at det kreves personlig klarering for å få tilgang


### Krav nr. 4.5.22 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Graderingskoder skal kunne ordnes hierarkisk, slik at det vil være mulig å angi at en bestemt gradering skal være mer eller mindre streng enn en annen bestemt gradering

#### Merknad
Obligatorisk hvis 4.5.21 oppfylles

### Krav nr. 4.5.23 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne angis tilgang til et konkret objekt for en bestemt bruker, uavhengig av øvrige avgrensninger i nedslagsfeltet (men fortsatt avhengig av brukerens funksjonelle rettigheter)



## Krav til tilgangsprofiler

### Krav nr. 4.5.24 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Innenfor hver av rollene som en bruker har, bør det kunne defineres en tilgangsprofil som utgjøres av rollens funksjonelle rettigheter i kombinasjon med nedslagsfeltet for rollen


### Krav nr. 4.5.25 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en påloggingsidentifikator har flere forskjellige tilgangsprofiler, bør vedkommende kunne velge blant de tilgangsprofilene som er definert for vedkommende


### Krav nr. 4.5.26 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør kunne byttes mellom tilgangsprofiler på en måte som oppleves som enkel for brukeren


### Krav nr. 4.5.27 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En av brukerens tilgangsprofiler bør kunne angis som standardprofil, som tilordnes ved pålogging hvis ikke annet angis særskilt


### Krav nr. 4.5.28 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å definere tilgangsprofiler som er slik at samme bruker kan ha definert forskjellige nedslagsfelter for en eller flere av sine roller



## Krav til tidsavgrensing og autorisasjonshistorie

### Krav nr. 4.5.29 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal lagres informasjon om hvilke tilgangsrettigheter en bruker har hatt, og når de var gyldige

#### Merknad
Obligatorisk for personlig identifikasjon

### Krav nr. 4.5.30 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangsrettigheter for en identifisert bruker skal kunne begrenses i tid, rettighetene må kunne gjelde fra dato til dato

#### Merknad
Obligatorisk for personlig identifikasjon

### Krav nr. 4.5.31 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgangsrettigheter bør kunne begrenses til en angitt tidssyklus, for eksempel tider på døgnet, dager i uka, kun arbeidsdager og lignende



## Krav til synliggjøring av brukeres autorisasjon

### Krav nr. 4.5.32 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en gitt, aktiv påloggingsidentifikator bør det være mulig å vise eller skrive ut en oversikt over hvilke rettigheter og fullmakter vedkommende har i Noark 5-løsningen


### Krav nr. 4.5.33 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å vise eller skrive ut oversikt over hvilke fullmakter en bestemt rolle eller tilgangsprofil har i løsningen


### Krav nr. 4.5.34 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For et gitt objekt i Noark 5-løsningen bør det være mulig å vise eller skrive ut hvilke brukere som har de ulike typene funksjonelle rettigheter til dette objektet



## Funksjonelle krav til gjenfinning

### Krav nr. 5.1.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes tjenester/funksjoner for å gjenfinne/søke fram metadata.


### Krav nr. 5.1.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved søking skal det være mulig å lage logiske sammenstillinger av metadata.


### Krav nr. 5.1.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved søk i metadata skal det være mulig å benytte venstre- og høyretrunkering samt markering av ett eller flere tegn i søkekriteriene.


### Krav nr. 5.1.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I metadataelementer som representerer datoer, skal det være mulig å søke på datointervaller.


### Krav nr. 5.1.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I metadataelementer som representerer datoer, skal det være mulig å søke på perioder som ligger før eller etter en gitt dato.


### Krav nr. 5.1.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å utføre fritekstsøk i metadata.


### Krav nr. 5.1.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Ved fritekstsøk i metadata, skal det være mulig å søke kombinert på flere søkeord ved hjelp av boolske operatorer.


### Krav nr. 5.1.8 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes tjenester/funksjoner for å gjenfinne/søke fram dokumenter.


### Krav nr. 5.1.9 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å gjenfinne dokumenter ut fra dokumentmetadata.


### Krav nr. 5.1.10 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å utføre fritekstsøk i et dokument hvis formatet legger til rette for det.


### Krav nr. 5.1.11 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Søkeresultat skal avspeile aktuell tilgang.


### Krav nr. 5.1.12 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Søkeresultat skal være nødvendig skjermet.


### Krav nr. 5.1.13 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulighet for at store og små bokstaver kan behandles som ekvivalente ved søk.


### Krav nr. 5.1.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes en tjeneste/funksjon for å avbryte søk som er satt i gang.


### Krav nr. 5.1.15 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Søkefunksjonene bør være innrettet slik at en ved søk på et ord i bokmålsform også får treff for de tilsvarende nynorskformene og omvendt.



## Krav til rapporten Løpende journal

### Krav nr. 5.2.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal valgfritt kunne selekteres på følgende metadataelementer (fra journalpost dersom ikke annet er angitt):

* journaldato (intervall skal kunne angis), eller

* løpenummer (intervall skal kunne angis)

* journalposttype (en eller flere skal kunne velges)

* journalenhet til saksbehandler

* administrativEnhet til saksbehandler

#### Merknad
Obligatorisk for sakarkiv

### Krav nr. 5.2.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold: Følgende metadataelementer skal være med i rapporten, så fremt de finnes i løsningen:
Saksmappeinformasjon
Fra Saksmappe:
mappeID
tittel
administrativEnhet
Saksansvarlig
referanseArkivdel

Fra klasse
klasseID og tittel
Journalpostinformasjon
Fra Journalpost:
løpenummer
registreringsID
journaldato
dokumentetsDato (tekst ”Udatert” hvis dato mangler)
tittel
tilgangsrestriksjon
skjermingshjemmel
antallVedlegg
offentlighetsvurdertDato
korrespondanseparttype
korrespondansepartnavn
administrativEnhet
saksbehandler
journalenhet

#### Merknad
Obligatorisk for sakarkiv


## Krav til rapporten Offentlig journal

### Krav nr. 5.2.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapporten skal inneholde alle journalposttyper. Registrering skal ikke være med.

#### Merknad
Obligatorisk for arkiv underlagt Offentleglova

### Krav nr. 5.2.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadataelementet skjermingMetadata inneholder informasjon om hvilke elementer som skal skjermes. Metadatafeltet offentligTittel er en kopi av tittel, men alle ord som skal skjermes er her fjernet (for eksempel erstattet av *****)

#### Merknad
Obligatorisk for arkiv underlagt Offentleglova

### Krav nr. 5.2.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal kunne selekteres på følgende metadataelementer (fra Journalpost hvis ikke annet er angitt):

* journaldato (intervall skal kunne angis)

* journalenhet

* administrativEnhet til saksbehandler

#### Merknad
Obligatorisk for arkiv underlagt Offentleglova

### Krav nr. 5.2.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For virksomheter som har tatt i bruk funksjonalitet for midlertidig sperring, skal rapporten som et alternativ til selektering etter journaldato, kunne selekteres etter metadataelementet:

* offentlighetsvurdert (jf. Journalpost). Intervall skal kunne angis.

#### Merknad
Obligatorisk for arkiv underlagt Offentleglova

### Krav nr. 5.2.9 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold:
Følgende metadataelementer skal være med i rapporten, så fremt de finnes i løsningen:
Saksmappeinformasjon
Fra Saksmappe:
mappeID
offentligTittel

Fra klasse (tilleggsklassering skal ikke være med):
klasseID (skrives ikke ut hvis markert som avskjermet i løsningen) 
Journalpostinformasjon
Fra Journalpost:
løpenummer
registreringsID
journaldato
dokumentetsDato (tekst ”Udatert” hvis dato mangler)
offentligTittel
korrespondanseparttype
korrespondansepartNavn (Skrives ikke ut i offentlig journal hvis navnet skal unntas offentlighet)
avskrivningsmåte
avskrivningsdato
referanseAvskrivesAvJournalpost
referanseAvskriverJournalpost

#### Merknad
Obligatorisk for arkiv underlagt Offentleglova

### Krav nr. 5.2.10 () Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapporten bør i tillegg valgfritt kunne inneholde en eller flere av opplysningene nedenfor (så fremt de finnes i løsningen):

Saksmappeinformasjon
Fra Saksmappe:
administrativEnhet
saksansvarlig
tilgangsrestriksjon
skjermingshjemmel

Journalpostinformasjon
Fra Journalpost (sortert etter registreringsID hvis ikke annet er angitt):
tilgangsrestriksjon
skjermingsHjemmel
administrativEnhet,
saksbehandler



## Krav til tilgangskoder for unntak fra offentlig journal

### Krav nr. 5.2.14 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal kunne registreres tilgangskode på mapper, registreringer og dokumentbeskrivelser. Den angir at registrerte opplysninger eller arkiverte dokumenter skal skjermes mot offentlighetens innsyn

#### Merknad
Obligatorisk for løsninger hvor informasjon skal unntas fra offentlighet

### Krav nr. 5.2.15 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle tilgangskoder som skal brukes må være forhåndsdefinert i kjernen. Tilgangskodene er globale, det vil si at de samme kodene brukes for hele arkivet uavhengig av hvilke eksterne moduler som gjør bruk av arkivet

#### Merknad
Obligatorisk for løsninger hvor informasjon skal unntas fra offentlighet

### Krav nr. 5.2.16 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kjernen skal inneholde full historikk over alle tilgangskoder som er eller har vært gyldige i arkivet

#### Merknad
Obligatorisk for løsninger hvor informasjon skal unntas fra offentlighet

### Krav nr. 5.2.17 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver tilgangskode skal det kunne registreres en indikasjon på hvorvidt et dokument som er merket med denne tilgangskoden kan unntas fra offentlighet i sin helhet, eller om det bare er anledning til å unnta bestemte opplysninger fra dokumentet i tråd med det som er angitt i offentleglovas hjemmelsbestemmelse

#### Merknad
Obligatorisk for løsninger hvor informasjon skal unntas fra offentlighet

### Krav nr. 5.2.18 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes en dedikert tilgangskode for ”midlertidig unntatt”, som kan brukes inntil skjermingsbehov er vurdert


### Krav nr. 5.2.19 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I tilknytning til en tilgangskode, skal følgende opplysninger knyttet til mappe i kjernen kunne markeres som ”skjermet” slik at eksterne moduler som leser fra arkivet får følgende begrensninger når tilgangskoden benyttes:

* Deler av mappetittelen: Løsningen skal enten tillate skjerming av alt unntatt første del av tittelen (for eksempel første linje), eller alternativt skjerming av enkeltord som bruker markerer

* Klassifikasjon: Dette er primært beregnet på skjerming av objektkoder som er personnavn eller fødselsnummer

* Opplysninger som identifiserer parter i saken

#### Merknad
Obligatorisk for løsninger hvor informasjon skal unntas fra offentlighet

### Krav nr. 5.2.20 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I tilknytning til en tilgangskode, skal følgende opplysninger knyttet til registreringer i kjernen kunne markeres som ”skjermet” slik at eksterne moduler som leser fra arkivet får følgende begrensninger når tilgangskoden benyttes:

* Deler av innholdsbeskrivelsen: Løsningen skal enten tillate skjerming av alt unntatt første del av innholdsbeskrivelsen (for eksempel første linje), eller alternativt skjerming av enkeltord som bruker markerer

* Opplysninger som identifiserer avsender og/eller mottaker


### Krav nr. 5.2.21 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dokumentbeskrivelser knyttet til en registrering skal kunne skjermes. Det skal fremgå at registreringen inneholder dokumentbeskrivelser som er skjermet i journalen


### Krav nr. 5.2.22 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Følgende opplysninger om elektroniske dokumenter skal kunne skjermes ved hjelp av tilgangskode:

* alle opplysninger om et dokument, innbefattet ulike formater og versjoner av dokumentet


### Krav nr. 5.2.23 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom tilgangskoden er merket med indikasjon på at det bare er anledning til å unnta visse opplysninger i dokumentet fra innsyn, kan det opprettes en ”offentlig variant” av dokumentet der disse opplysningene ikke finnes, som derfor kan unntas fra skjerming



## Krav til skjermingsfunksjoner og – metoder for unntak fra offentlig journal

### Krav nr. 5.2.24 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør synliggjøres i journalen om en registrering med en tilgangskode inneholder ett eller flere dokumenter som ikke er merket med tilgangskode


### Krav nr. 5.2.25 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom tilgangskoden er merket med indikasjon på at det bare er anledning til å unnta visse opplysninger i dokumentet fra innsyn, kan det opprettes en ”offentlig variant” av dokumentet der disse opplysningene ikke finnes, som derfor kan unntas fra skjerming


### Krav nr. 5.2.26 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Løsningen bør vise hvilke opplysningstyper som er angitt at skal skjermes. Det at en gitt opplysning er avkrysset for skjerming bør vises både for de som har tilgang til å se de skjermede opplysningene og for de som ikke har tilgang til å se dem


### Krav nr. 5.2.27 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dokumentbeskrivelsen bør arve registreringens tilgangskode som standardverdi, dersom ikke dokumentbeskrivelsen har tilgangskode fra før, og dersom den ikke fra før er tilknyttet en annen registrering



## Krav til tilgjengeliggjøring av offentlig journal på Internett

### Krav nr. 5.2.28 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å eksportere uttrekk for tilgjengeliggjøring av offentlig journal.


### Krav nr. 5.2.29 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Innholdet i offentlig journal tilgjengeliggjort på Internett skal samsvare med arkivforskriften § 10 første ledd annet punktum. I tillegg skal det være med et kontakt­punkt som publikum kan henvende seg til hos organet. Se for øvrig offentlegforskrifta § 6

#### Merknad
Obligatorisk hvis løsningen muliggjør tilgjengeliggjøring på Internett

### Krav nr. 5.2.30 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Offentlig journal på Internett skal ikke inneholde informasjon som er unntatt fra offentlighet. Denne informasjonen skal allerede være skjermet i løsningen.

#### Merknad
Obligatorisk hvis løsningen muliggjør tilgjengeliggjøring på Internett

### Krav nr. 5.2.31 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Følgende informasjon skal aldri gjøres tilgjengelig på Internett, selv om informasjonen ikke er unntatt offentlighet: 

* Opplysninger nevnt i personvernforordningen artikkel 9 og 10

* Fødselsnummer, personnummer og nummer med tilsvarende funksjon

* Opplysninger om lønn og godtgjøring til fysiske personer, bortsett fra opplysninger om lønn og godtgjøring til personer i ledende stillinger 

* Materiale som tredjepart har immaterielle rettigheter til (bortsett fra søknader, argumentasjonsskriv, høringsuttalelser og lignende vanlig materiale sendt i forbindelse med en sak).

#### Merknad
Obligatorisk hvis løsningen muliggjør tilgjengeliggjøring på Internett

### Krav nr. 5.2.32 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Personnavn som tilgjengeliggjøres direkte på en webside bør merkes for utelukking fra indeksering av indekseringstjenester.


### Krav nr. 5.2.33 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Personnavn som tilgjengeliggjøres bør ikke være søkbare etter ett år.


### Krav nr. 5.2.34 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Personnavn bør merkes med XML-taggene <personnavn> </personnavn> før de eksporteres.


### Krav nr. 5.2.35 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å tilgjengeliggjøre arkivdokumenter knyttet til de enkelte journalpostene i offentlig journal på Internett.


### Krav nr. 5.2.36 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Arkivdokumenter som inneholder informasjon nevnt i offentlegforskrifta § 7, skal ikke tilgjengeliggjøres på Internett. (Dette betyr normalt at tilgjengeliggjøring av dokumenter ikke kan automatiseres, en må ta stilling til tilgjengeliggjøring i hvert enkelt tilfelle.)

#### Merknad
Obligatorisk dersom løsningen muliggjør tilgjengeliggjøring av arkiv-dokumenter på Internett

### Krav nr. 5.2.37 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom arkivdokumenter tilgjengeliggjøres på Internett, skal det i Internettløsningen opplyses om hvilket kriterium som ligger til grunn for utvalget av dokumenter, jf. Offentlegforskrifta § 7 siste ledd.

#### Merknad
Obligatorisk dersom løsningen muliggjør tilgjengeliggjøring av arkiv-dokumenter på Internett

### Krav nr. 5.2.38 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tilgjengeliggjøring av offentlig journal og eventuelle arkivdokumenter på Internett bør etableres med hindre mot automatisert indeksering fra eksterne aktører, f.eks. søkemotorer.



## Krav til sikring av partsinnsyn 

### Krav nr. 5.2.39 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en part som krever innsyn etter forvaltningsloven skal det kunne gis utskrift av alle metadata og dokumenter i den bestemte saken. Opplysninger skal vises selv om de er påført tilgangskoder


### Krav nr. 5.2.40 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For en person som krever innsyn etter personopplysningsloven skal det kunne gis utskrift av alle metadata om de saker hvor vedkommende er part i saken, og de registreringer med tilhørende dokumenter og merknader der vedkommende selv er avsender eller mottaker. Eventuelle skjermede opplysninger om andre parter i saken skal skjermes i utskriften


### Krav nr. 5.2.41 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en person er autentisert som ekstern bruker, bør vedkommende selv kunne hente ut de opplysninger vedkommende har rett til innsyn i som part eller som registrert person gjennom tilrettelagt fagsystem eller innsynsløsning



## Funksjonelle krav til bevaring og kassasjon

### Krav nr. 6.1.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata om bevaring og kassasjon på en klasse skal kunne arves til mappe, registrering og dokumentbeskrivelse.

#### Merknad
Obligatorisk hvis kassasjon er aktuelt

### Krav nr. 6.1.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata om bevaring og kassasjon på en arkivdel skal kunne arves til mappe, registrering og dokumentbeskrivelse.

#### Merknad
Obligatorisk hvis kassasjon er aktuelt

### Krav nr. 6.1.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom arv av metadata om bevaring og kassasjon skal skje fra arkivdel, skal dette overstyre arv av metadata fra klassene.

#### Merknad
Obligatorisk hvis kassasjon er aktuelt

### Krav nr. 6.1.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en tjeneste / funksjon for å registrere et kassasjonsvedtak for en mappe, registrering eller dokumentbeskrivelse.
Kassasjonsvedtaket skal bestå av følgende obligatoriske verdier:
1. Bevares
2. Kasseres
3. Vurderes senere
Andre verdier kan legges til.

#### Merknad
Obligatorisk for påføring av kassasjonsvedtak utover arkivdel og klasse.

### Krav nr. 6.1.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig manuelt å registrere kassasjonsvedtak, kassasjonshjemmel og bevaringstid for en mappe, registrering eller dokumentbeskrivelse.

#### Merknad
Obligatorisk hvis 6.1.4 oppfylles

### Krav nr. 6.1.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Bevaringsdatoen for en mappe, registrering eller dokumentbeskrivelse skal kunne beregnes automatisk på grunnlag av bevaringstid og datoen mappen ble avsluttet.

#### Merknad
Obligatorisk hvis 6.1.4 oppfylles

### Krav nr. 6.1.7 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Andre regler for beregning av bevaringsdato bør kunne være mulig.


### Krav nr. 6.1.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Bevaringsdato for en mappe, registrering eller dokumentbeskrivelse skal også kunne registreres manuelt. Bevaringstid er da ikke obligatorisk.

#### Merknad
Obligatorisk hvis 6.1.4 oppfylles

### Krav nr. 6.1.9 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å slå av funksjonen for arv fra klasser og arkivdeler, slik at metadata om bevaring og kassasjon ikke arves til underliggende mapper.

#### Merknad
Obligatorisk for funksjon for arv av kassasjonskode

### Krav nr. 6.1.10 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å angi at arv av metadata om bevaring og kassasjon også skal gå ned til registrering og dokumentbeskrivelse.

#### Merknad
Obligatorisk for funksjon for arv av kassasjonskode

### Krav nr. 6.1.11 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata om bevaring og kassasjon som arves fra et arkivobjekt til alle underliggende arkivobjekter, skal kunne overskrives.

#### Merknad
Obligatorisk for funksjon for arv av kassasjonskode

### Krav nr. 6.1.12 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør finnes en tjeneste/funksjon som automatisk knytter en bestemt type registreringer eller dokumentbeskrivelser til et bevarings- og kassasjonsvedtak.


### Krav nr. 6.1.13 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata om bevaring og kassasjon skal da arves til alle opprettede registreringer eller dokumentbeskrivelser av samme type.

#### Merknad
Obligatoriske hvis 6.1.12 oppfylles

### Krav nr. 6.1.14 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å få presentert en oversikt over dokumenter som skal kasseres etter et bestemt tidspunkt. En slik oversikt skal kunne begrenses til et mindre utvalg dokumenter.


### Krav nr. 6.1.15 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å få presentert en oversikt over dokumenter som skal vurderes på nytt for bevaring eller kassasjon etter et bestemt tidspunkt. En slik oversikt skal kunne begrenses til et mindre utvalg dokumenter.


### Krav nr. 6.1.16 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Oversikten skal inneholde de viktigste metadata for dokumentene, inkludert metadata for bevaring og kassasjon.


### Krav nr. 6.1.17 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å åpne et dokument for presentasjon av innhold direkte fra denne oversikten.


### Krav nr. 6.1.18 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Autoriserte brukere bør kunne endre metadata for bevaring og kassasjon for de enkelte dokumenter direkte fra oversikten.


### Krav nr. 6.1.19 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal finnes en funksjon for å kassere alle dokumenter som har verdien "Kasseres" som kassasjonsvedtak, og hvor bevaringsdatoen er eldre enn dagens dato. En slik funksjon skal kunne begrenses til et mindre utvalg dokumenter.

#### Merknad
Obligatorisk i løsninger hvor kassasjon skal skje og ved behov for skille mellom kassable og ikke kassable dokumenter.

### Krav nr. 6.1.20 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal ikke være mulig å sette kassasjonsvedtak "Kasseres" på en mappe som er registrert som presedenssak.


### Krav nr. 6.1.21 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kassasjonen skal kunne utføres automatisk for hele utvalget dokumenter, men det skal også være mulig å be om spørsmål om kassasjon skal utføres for hvert enkelt dokument.

#### Merknad
Obligatorisk når 6.1.19 oppfylles

### Krav nr. 6.1.22 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Bare autoriserte brukere kan starte en funksjon for kassasjon av dokumenter.


### Krav nr. 6.1.23 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle versjoner, varianter og formater av dokumentet skal omfattes av kassasjonen.


### Krav nr. 6.1.24 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Kassasjon skal innebære at all metadata om dokumentobjektet slettes. Selve dokumentet skal slettes fra filsystemet dersom dokumentet (dokumentbeskrivelsen) ikke er knyttet til andre registreringer.


### Krav nr. 6.1.25 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Funksjonen for kassasjon bør være i to trinn, slik at det i første omgang er mulig å gjenopprette de kasserte dokumentene. Endelig sletting av dokumentobjekt og dokument skal kunne skje på et senere tidspunkt.


### Krav nr. 6.1.26 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata om dokumentet ned til dokumentbeskrivelse, skal i utgangspunktet ikke slettes selv om dokumentet kasseres.


### Krav nr. 6.1.27 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hvert dokument som blir kassert, skal det på dokumentbeskrivelsesnivå logges dato for kassasjon og hvem som utførte kassasjonen.



## Krav til rapporten Kassasjonsliste

### Krav nr. 6.1.28 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal kunne selekteres på følgende metadataelementer i Saksmappe:

* kassasjonsdato (intervall skal kunne angis)

* kassasjonsvedtak

* administrativEnhet (Her skal det kunne angis om underliggende enheter skal inkluderes)

* journalenhet.

* referanseArkivdel

* arkivperiodeStartDato og arkivperiodeSluttDato fra arkivdel

#### Merknad
Obligatorisk for løsninger som skal legge til rette for kassasjon

### Krav nr. 6.1.29 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapporten skal inneholde følgende opplysninger, så fremt de finnes i løsningen:
Saksmappeinformasjon
Fra Saksmappe:
mappeID
tittel
opprettetDato
kassasjonsvedtak
kassasjonsdato
administrativEnhet
referanseArkivdel
Fra klasse
klasseID og tittel
Fra arkivdel:
referanseForelder
arkivperiodeStartDato
arkivperiodeSluttDato

#### Merknad
Obligatorisk for løsninger som skal legge til rette for kassasjon


## Strukturelle krav til periodisering

### Krav nr. 6.2.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivdel skal kunne inneholde en tekstlig beskrivelse av hvilke prinsipper den skal periodiseres etter.


### Krav nr. 6.2.2 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivdel skal inneholde referanser til eventuelle forløpere og arvtakere. (forgjengere og etterkommere?)



## Funksjonelle krav til periodisering

### Krav nr. 6.2.3 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å knytte nyopprettede mapper til en arkivdel som inneholder en aktiv arkivperiode.


### Krav nr. 6.2.4 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivdel som inneholder en overlappingsperiode, skal være sperret for tilføyelse av nyopprettede mapper. Men eksisterende mapper i en overlappingsperiode skal være åpne for nye registreringer.


### Krav nr. 6.2.5 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom en ny registrering føyes til en mappe som tilhører en arkivdel i overlappingsperiode, skal mappen automatisk overføres til arkivdelens arvtaker.


### Krav nr. 6.2.6 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En arkivdel som inneholder en avsluttet arkivperiode, skal være sperret for tilføyelse av nye mapper. Alle mapper skal være lukket, slik at heller ingen registreringer og dokumenter kan føyes til.


### Krav nr. 6.2.7 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være umulig å avslutte en arkivdel i overlappingsperiode dersom den fremdeles inneholder åpne mapper.


### Krav nr. 6.2.8 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å få en oversikt over mapper som fremdeles er åpne i en overlappingsperiode.


### Krav nr. 6.2.9 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å overføre åpne mapper fra en arkivdel i en overlappingsperiode til arkivdelens arvtaker.


### Krav nr. 6.2.10 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å overføre åpne mapper fra en arkivdel i en samlet, automatisert prosess.


### Krav nr. 6.2.11 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å flytte avsluttede mapper til en arkivdel som inneholder en avsluttet periode.

#### Merknad
Obligatorisk for funksjon for periodisering

### Krav nr. 6.2.12 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom dokumentene i en arkivdel er ikke-elektroniske (fysiske), skal det også være mulig å registrere oppbevaringssted.



## Krav til migrering mellom Noark-løsninger

### Krav nr. 6.3.1 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å eksportere alle metadata som er definert i denne standarden med tilhørende dokumenter basert på avleveringsformatet.


### Krav nr. 6.3.2 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å importere alle metadata som er definert i denne standarden med tilhørende dokumenter basert på avleveringsformatet.


### Krav nr. 6.3.3 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å eksportere deler av arkivstrukturen, f.eks. en arkivdel eller en klasse.


### Krav nr. 6.3.4 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å importere deler av arkivstrukturen, f.eks. en arkivdel eller en klasse.


### Krav nr. 6.3.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal produseres en logg over alle metadataelementer og dokumenter som ikke kan importeres og over andre feil som eventuelt oppstår under importen.

#### Merknad
Obligatorisk ved import

### Krav nr. 6.3.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når det foretas en import skal det genereres en loggfil med informasjon om hvordan importen har gått, f.eks. antall metadataelementer og dokumenter. Loggfilen skal også inneholde en liste over alle metadataelementer og dokumenter som det ikke har vært mulig å importere.

#### Merknad
Obligatorisk ved import


## Overordnede krav til arkivuttrekk

### Krav nr. 6.4.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det skal være mulig å produsere arkivuttrekk bestående av arkivdokumenter, journalrapporter, metadata, endringslogg og XML-skjemaer.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Arkivuttrekket skal utgjøre en avleveringspakke (Submission Information Packages), slik dette er definert i ISO 14571 OAIS.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Formatet på metadata, endringslogg og journalrapporter i arkivuttrekket skal være XML (XML 1.0).

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Tegnsettet til alle XML-filer skal være UTF-8.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.5 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadataelementer som ikke har verdi, skal utelates fra arkivuttrekket. I uttrekket skal det med andre ord ikke forekomme tomme elementer med kun start- og slutt-tagg.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.6 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alfanumeriske verdier i arkivuttrekket skal representeres vha. XML Schema 1.0 -datatypen string.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.7 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Datoer uten klokkeslett i arkivuttrekket skal representeres vha. XML Schema 1.0 -datatypen date.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.8 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Datoer med klokkeslett i arkivuttrekket skal representeres vha. XML Schema 1.0 -datatypen dateTime.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.9 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Heltall i arkivuttrekket skal representeres vha. XML Schema 1.0-datatypen integer.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.10 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Format på arkivdokumenter i arkivuttrekket skal være et av arkivformatene definert i § 5-17 i riksarkivarens forskrift.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.11 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Organiseringen av filene i arkivuttrekket skal følge riksarkivarens forskrift kapittel 5, så langt disse er relevante.

#### Merknad
Obligatorisk ved avlevering til arkivdepot


## Krav til innholdet i en avleveringspakke

### Krav nr. 6.4.12 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Et arkivuttrekk skal omfatte en avsluttet arkivperiode, og bestå av innholdet i en eller flere avsluttede arkivdeler.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.13 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hele klassifikasjonsstrukturen, dvs. alle klasser i et klassifikasjonssystem, skal inngå i hver enkelt avleveringspakke. Sekundære klassifikasjonssystemer kan også være med, men klassene her skal ikke inneholde mapper.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.14 (V) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Det bør være mulig å produsere et arkivuttrekk på grunnlag av en startdato og en sluttdato, uavhengig av tilhørighet til arkivdel og om mappene er avsluttet eller ikke.

#### Merknad
Kravet gjelder særlig ved migrering.

### Krav nr. 6.4.15 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Filene i en avleveringspakke skal ligge under en felles overordnet filkatalog kalt avleveringspakke.

Avleveringspakken skal inneholde følgende filer:

* arkivuttrekk.xml  (dokumentasjon av innholdet i  arkivuttrekket)

* arkivstruktur.xml (metadata om dokumentene)

* endringslogg.xml (logging av endrede metadata)

Dersom avleveringspakken inneholder arkivuttrekk med journalføringspliktig informasjon, skal den i tillegg inneholde følgende filer:

* loependeJournal.xml

* offentligJournal.xml

XML-skjemaene til alle XML-filer i avleveringspakken skal også være inkludert.  For virksomhetsspesifikke metadata skal det medfølge egne XML-skjemaer. 

Dokumentene skal ligge i en underkatalog kalt DOKUMENT. Denne katalogen kan struktureres i nye underkataloger etter fritt valg. Dokumentfilene endelse skal angi arkivformat: pdf, tif, txt osv.

#### Merknad
Obligatorisk ved avlevering til arkivdepot


## Krav til XML-skjemaene+B452

### Krav nr. 6.4.16 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle XML-filer som inngår i en avleveringspakke, skal være definert vha. medfølgende XML-skjema.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.17 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
XML-skjemaene skal følge XML skjema-standarden XML Schema 1.0


### Krav nr. 6.4.18 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For arkivuttrekk.xml, arkivstruktur.xml, endringslogg.xml, loependeJournal.xml og offentligJournal.xml skal kun de tilhørende skjemaene som er tilgjengelige fra Arkivverket, benyttes i avleveringspakken. Varianter av skjemaene skal ikke benyttes.


### Krav nr. 6.4.19 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Navngivingen i skjemaene slik det er vist i tabellen over XML-filer og tilhørende skjemaer, er obligatorisk.



## Krav til opplysninger om avleveringen

### Krav nr. 6.4.20 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Filene arkivuttrekk.xml og addml.xsd skal være med som en del av arkivuttrekket.

#### Merknad
Obligatorisk ved produksjon av arkivuttrekk

### Krav nr. 6.4.21 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I arkivuttrekk fra Noark 5-løsninger skal struktur og innhold i arkivuttrekk.xml være i henhold til Riksarkivarens Noark 5-mal for arkivuttrekk.xml.

#### Merknad
Obligatorisk ved produksjon av arkivuttrekk

### Krav nr. 6.4.22 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Følgende typer informasjon skal med i arkivuttrekk.xml:
 

* Arkivskapernavn

* Navn på systemet/løsningen

* Navn på arkivet

* Start- og sluttdato for arkivuttrekket

* Hvilken type periodisering som er utført i forrige periode og denne periode

* Opplysning om det finnes skjermet informasjon i uttrekket

* Opplysning om uttrekket omfatter dokumenter som er kassert

* Opplysning om uttrekket inneholder dokumenter som skal kasseres på et senere tidspunkt

* Opplysning om det finnes virksomhetsspesifikke metadata i arkivstruktur.xml

* Antall mapper i arkivstruktur.xml

* Antall registreringer i arkivstruktur.xml, loependeJournal.xml og offentligJournal.xml

* Antall dokumentfiler i uttrekket

* Sjekksummer for alle XML-filer og XML-skjemaer i arkivuttrekket, unnttatt arkivuttrekk.xml og addml.xsd

#### Merknad
Obligatorisk ved produksjon av arkivuttrekk

### Krav nr. 6.4.23 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For uttrekk hvor arkivstruktur.xml inneholder virksomhetsspesifikke metadata, skal informasjon om de XML-skjemaene som definerer disse være med i arkivuttrekk.xml. Denne informasjonen skal være i strukturen under dataobjektet arkivstruktur på samme måte som de øvrige skjemaene til arkivstruktur.

#### Merknad
Obligatorisk ved produksjon av arkivuttrekk


## Krav til metadata i arkivuttrekket

### Krav nr. 6.4.24 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En avleveringspakke skal inneholde en fil med metadata for arkivdokumentene som inngår i pakken.  Alle metadataelementene skal være nøstet inn i en sammenhengende, hierarkisk struktur.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.25 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle metadataelementer som er merket med "A" i kolonnen "Avl." i vedlegget "Metadata gruppert på objekter" skal være med i arkivuttrekket, såfremt de er tilordnet verdier.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.26 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle forekomster av arkivenheter i arkivstrukturen skal være identifisert med en entydig identifikasjon. Denne identifikasjonen skal være entydig for alle arkivuttrekk som produseres av en arkivskaper.

#### Merknad
Obligatorisk ved avlevering til arkivdepot

### Krav nr. 6.4.27 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Metadata for arkivdokumenter som er kassert før arkivuttrekket produseres, skal være med i uttrekket. Disse metadataene skal omfatte alle arkivenheter ned til dokumentbeskrivelse, og her skal det også ligge logginformasjon om kassasjonen.

#### Merknad
Obligatorisk for sakarkiver.


## Krav til Endringslogg

### Krav nr. 6.4.28 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En avleveringspakke skal inneholde en endringslogg for metadata som har fått en ny verdi.  Hvilke metadata dette gjelder, og når logging av disse endringene skal utføres, går fram av vedlegg 3 "Oversikt over metadata hvor det skal logges at det gjøres endringer i innholdet.

#### Merknad
Obligatorisk ved avlevering til arkivdepot


## Krav til journalrapportene

### Krav nr. 6.4.29 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En avleveringspakke skal inneholde både en løpende journal og en offentlig journal. Journalene skal omfatte samme tidsrom som arkivperioden.

#### Merknad
Obligatorisk for arkiver med korrespondanse-dokumenter som det kan være aktuelt å avlevere til arkivdepot

### Krav nr. 6.4.30 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Journalrapportene skal inneholde alle registreringer av typen journalpost som er journalført i løpet av arkivperioden. Journalpostene skal være sortert kronologisk etter løpenummer (journalår og sekvensnummer).

#### Merknad
Obligatorisk for arkiver med korrespondanse­dokumenter som det kan være aktuelt å avlevere til arkivdepot


## Krav til virksomhetsspesifikke metadata

### Krav nr. 6.4.31 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hvis virksomhetsspesifikke metadata skal inngå som en del av arkivuttrekket, skal de knyttes til mappe, registrering eller sakspart i arkivstruktur.xml gjennom elementet virksomhetsspesifikkeMetadata.

#### Merknad
Obligatorisk ved bruk av virksomhets-spesifikke metadata

### Krav nr. 6.4.32 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Alle virksomhetsspesifikke metadataelementer skal være definert i ett eller flere medfølgende XML-skjemaer.

#### Merknad
Obligatorisk ved bruk av virksomhets-spesifikke metadata

### Krav nr. 6.4.33 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Når virksomhetsspesifikke metadata inngår som en del av arkivuttrekket, skal det finnes referanse til aktuelle skjemaer i arkivstruktur.xml.

#### Merknad
Obligatorisk ved bruk av virksomhets-spesifikke metadata

### Krav nr. 6.4.34 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Virksomhetsspesifikke metadataelementer skal være tilordnet et namespace gjennom tilhørende XML-skjema.

#### Merknad
Obligatorisk ved bruk av virksomhets-spesifikke metadata

### Krav nr. 6.4.35 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Innholdet og betydningen av hvert virksomhetsspesifikt metadataelement skal dokumenteres mer inngående i aktuelt XML skjema dersom det ikke er innlysende hva elementet inneholder. Denne dokumentasjonen skal være i form av XML Schema elementene annotation og documentation knyttet til definisjonen av det enkelte metadataelementet i aktuelt skjema.

#### Merknad
Obligatorisk ved bruk av virksomhets-spesifikke metadata


## Krav til arkivdokumentene

### Krav nr. 6.4.36 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
En avleveringspakke skal inneholde arkivdokumenter i arkivformat. Hvert dokument skal eksporteres som én dokumentfil

#### Merknad
Obligatorisk ved avlevering av elektroniske arkivdokumenter til arkivdepot

### Krav nr. 6.4.37 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hver arkivert versjon av et dokument skal eksporteres som en egen dokumentfil.

#### Merknad
Obligatorisk ved avlevering av elektroniske arkivdokumenter til arkivdepot

### Krav nr. 6.4.38 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hver arkivert variant av et dokument skal eksporteres som en egen dokumentfil.

#### Merknad
Obligatorisk ved avlevering av elektroniske arkivdokumenter til arkivdepot

### Krav nr. 6.4.39 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
I et sakarkiv skal også dokumenter som er knyttet til registreringer av typen registrering (dvs. dokumenter som er arkivert uten journalføring) inngå i arkivuttrekket.

#### Merknad
Obligatorisk for sakarkiver hvor dokumenter er arkivert uten journalføring.

### Krav nr. 6.4.40 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hvert dokumentobjekt i arkivstruktur.xml skal ha en referanse til en dokumentfil i avleveringspakken. Det skal ikke forekomme referanser til dokumenter som ikke finnes i pakken, og det må ikke være dokumenter i pakken som det ikke blir referert til. Referansen fra arkivstrukturen skal være relativ til dokumentfilene, dvs. inneholde hele "stien" til dokumentet.

#### Merknad
Obligatorisk ved avlevering av elektroniske arkivdokumenter til arkivdepot

### Krav nr. 6.4.41 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hvert dokumentobjekt i arkivstruktur.xml skal inneholde informasjon om dokumentets format og størrelse. Det skal også inneholde sjekksummen for hvert enkelt dokument, samt algoritmen som ble brukt for å generere sjekksummen.

#### Merknad
Obligatorisk ved avlevering av elektroniske arkivdokumenter til arkivdepot

### Krav nr. 6.4.42 (O) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Dersom dokumentet er blitt konvertert fra et format til et annet (f.eks. fra produksjonsformat til arkivformat) skal det tilhørende dokumentobjektet i arkivstruktur.xml inneholde informasjon om konverteringen. Er dokumentet blitt konvertert flere ganger, skal alle konverteringene dokumenteres.



## Krav til rapporten Liste for bortsetting, avlevering og overføring

### Krav nr. 6.5.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal valgfritt kunne selekteres på følgende metadataelementer:

* arkivperiodeStartDato og arkivperiodeSluttDato fra arkivdel (en eller flere), eller

* referanseArkivdel fra Saksmappe (en eller flere).

* journalenhet fra Saksmappe (en eller flere)

* administrativEnhet fra Saksmappe (Her skal det kunne angis om underliggende enheter skal inkluderes.)

* saksstatus i Saksmappe

* avskrivningsdato fra Journalpost (Her skal også verdien ”tomt felt” kunne angis)

* kassasjonsvedtak

#### Merknad
Obligatorisk for løsninger som skal foreta bortsetting, avlevering og overføring

### Krav nr. 6.5.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold:
Rapporten skal inneholde følgende opplysninger, så fremt de finnes i
løsningen:
Saksmappeinformasjon
Fra Saksmappe:
mappeID
opprettetdato
tittel
saksstatus
kassasjonsvedtak
kassasjonsdato
referanseArkivdel
Fra klasse
klasseID og tittel
Fra arkivdel:
referanseArkiv
arkivperiodeStartDato
arkivperiodeSluttDato

#### Merknad
Obligatorisk for løsninger som skal foreta bortsetting, avlevering og overføring

### Krav nr. 6.5.3 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
For hver ny klasseID skal klassifikasjonssystemets tekst (det avledete metadataelementet tittel) tas med på en egen linje som overskrift.

#### Merknad
Obligatorisk for løsninger som skal foreta bortsetting, avlevering og overføring

### Krav nr. 6.5.4 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Hvis rapporten inneholder dokumenter som er gradert, skal antall graderte dokumenter markeres ved saken.

#### Merknad
Obligatorisk for løsninger som skal foreta bortsetting, avlevering og overføring


## Krav til rapporten Arkivoversikt

### Krav nr. 6.6.1 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Selektering:
Rapporten skal valgfritt kunne selekteres etter metadataelementene:

* referanseForelder i arkivdel, eller

* arkivperiodeStartDato og arkivperiodeSluttDato i arkivdel

#### Merknad
Obligatorisk for sakarkiver

### Krav nr. 6.6.2 (B) Grad av oppfyllelse: Ja/Nei/IA

#### Beskrivelse
Rapportens innhold:  Følgende metadataelementer skal være med i rapporten, så fremt de finnes i løsningen:
Fra arkiv:
tittel
arkivskapernavn
arkivstatus
opprettetDato
avsluttetDato
Fra klassifikasjonssystem
Klassifikasjonstype
tittel
Fra arkivdel:
tittel
referanseForelder
referanseKlassifikasjonssystem
arkivdelstatus
referanseArvtaker
referanseForløper
fysiskeDokumenter
referanseDokumentbeskrivelse
opprettetDato
avsluttetDato
arkivperiodeStartDato
arkivperiodeSluttDato
oppbevaringssted
beskrivelse
eksportertDato
ansvarligEksport

#### Merknad
Obligatorisk for sakarkiver

