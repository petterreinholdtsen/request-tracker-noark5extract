# Vedlegg til egenerklæringsskjemaet

Skjemaet skal fylles ut når løsningen det søkes om Noark 5-godkjenning
for, ikke tilfredsstiller alle O-krav og B-krav og ikke inneholder
alle metadataelementer som kan forventes oppfylt ut fra type løsning.

| Kravnummer/Metadata nr.   | Kommentar/ begrunnelse |
| ------------------------- | ---------------------- |
|                           |                        |
